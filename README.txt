1 - Criar o ambiente virtual para a aplicação
    make build

2 - Ativar o ambiente virtual
    source .venv/bin/activate

3 - Executar o gerenciador com um dos dois comandos
    make run 
        OU
    python manage.py runserver 0.0.0.0:8000
