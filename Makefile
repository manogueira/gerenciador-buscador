run:
	python manage.py runserver 0.0.0.0:8000


build:
	python3.8 -m venv ~/venvs/gerenciador
	ln -s ~/venvs/gerenciador .venv
	(\
	. .venv/bin/activate; \
	pip install --upgrade pip; \
	pip install -r requirements.txt; \
	)

remove:
	rm .venv
	rm -r ~/venvs/gerenciador

####
#ERRO DE LOCALE LC_ALL VAZIO
#####echo 'export LC_ALL="en_US.UTF-8"' >> ~/.bashrc && source ~/.bashrc
