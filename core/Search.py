from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import subprocess
import threading
import time
import requests
import datetime
import pytz
import asyncio
import time
#from ipware.ip import get_ip
import json
import os
import base64
from ipware import get_client_ip


def get_request_info(request):
	info = get_client_ip(request)
	return info

def log(_obj):	
	import json
	import uuid		
	obj = json.dumps(_obj)		
	
	u = uuid.uuid1()
	id = u.urn.split(':')[-1]

	if not os.path.exists('/dados/logs/'):
		os.makedirs('/dados/logs/')
	
	with open('/dados/logs/'+id,'w') as f:
		f.write(obj)
def log_insert(_obj):	
	import json
	import uuid		
	obj = json.dumps(_obj)		
	
	u = uuid.uuid1()
	id = u.urn.split(':')[-1]
	if not os.path.exists('/dados/logs-insert/'):
		os.makedirs('/dados/logs-insert/')
	
	with open('/dados/logs-insert/'+id,'w') as f:
		f.write(obj)

def saveDocument(_fileName,document,):
	##Salvando o documento recebido	
	# document['content'] = base64.b64decode(document['content']).decode('utf-8').replace("'","\"")
	# document['content'] = json.loads(document['content'])
	# print(document['content'])
	_file = open(_fileName+'.txt','w')
	for feature in document['content']:
		_file.write("{} {}\n".format(feature,document['content'][feature]))
	_file.close()
	
def saveConf(_fileName,_conf):
	_file = open(_fileName+'.csv','w')
	_file.write(_conf+'\n')
	_file.close()

@csrf_exempt
def search(request):
	now = datetime.datetime.now()
	date = '{}/{}/{} {}:{}'.format(now.day,now.month,now.year,now.hour,now.minute)

	if request.method == 'GET':		
		ip = get_request_info(request)
		
		broker_ip = '201.73.0.34'
		request_ip = ip
		query = request.GET.urlencode()
		
		action = request.GET.get('action')
		ip = 'http://'+broker_ip+':8000/broker/?'

		if action == 'search':
			t1 = time.time()
			f = requests.get(ip+query)
			
			obj = {}
			obj['request_ip'] = request_ip
			obj['datetime'] = date
			obj['query'] = query
			
			result = f.json()
			obj['result'] = result['documents']
			num = 100
			if request.GET.get('numResp'):
				if int(request.GET.get('numResp')) != 100:
					num = int(request.GET.get('numResp'))
				if num == -1:
					num = len(result['documents'])
			print(num)
			if(len(result['documents']) > num):
				aux= []
				for i in range(0,num):
					aux.append(result['documents'][i])
				result['documents'] = aux[:]
			#result['documents'] = result['documents'][:]			
			# log(obj)
			t2 = time.time()
			obj['time'] = t2-t1
			print('Search - Time elapsed:',obj['time'])
			# obj['result'] = result['documents']
			return JsonResponse(result)
		
		elif action == 'info':
			f = requests.get(ip+query)			
			return JsonResponse(f.json())
		elif action == 'teste':
			return JsonResponse()

	elif request.method == 'POST':
		path = '/dados/to-index/'
		log_path = '/dados/'
		if not os.path.exists(path):
			os.makedirs(path)
		data = request.body.decode('utf-8')
		received_json_data = json.loads(data)
		
		if received_json_data['action'] == 'index':			
			database = received_json_data['database']
			if not os.path.exists(path+database):
				os.makedirs(path+database)
			document = received_json_data['document']
			doc_name = document['name'].split('.')[0]
			new_doc = '{}{}/{}'.format(path,database,doc_name)
			conf = '{}.txt;{};{}'.format(new_doc,document['name'],document['link'])
			saveDocument(new_doc,document)
			saveConf(new_doc,conf)		
			
			print(date)
			_log = {"status":'OK', "documet" : document, "date" : date}
			log_insert(_log)			
			return JsonResponse({"status":'OK'})
