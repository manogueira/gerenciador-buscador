from django.http import JsonResponse
from django.http import HttpResponse
import subprocess

import time


class Slave(object):
	"""docstring for Slave"""
	def __init__(self, arg):
		super(Slave, self).__init__()
		self.arg = arg

	def search(self,_query):
		gStart = time.time()
		_query['query'] = _query['query'].replace(' ','_')

		cont = subprocess.run(['aLine','-q',_query['query'],'-d','/dados/'+_query['database']],stdout=subprocess.PIPE).stdout.decode('utf-8')
		cont = cont.split('\n')
		#print(cont)
		result = {}
		for i in cont:
			i = i.replace('  ',' ')
			i = i.split(' ')
			if len(i) > 2:
				#print(i)
				doc = {}
				doc['document'] = i[0]
				doc['rank'] = float(i[2])
				doc['data'] = ' '
				result[i[1]] = doc

			#ADICIONAR MAIS DADOS NESSE JSON
		gEnd = time.time()
		elapsed = gEnd - gStart

		if len(result) > 0:
			status = True
			return JsonResponse({'result':result,'time':elapsed,'status':status})
		else:
			status = False
			return JsonResponse({'status':status})

	def report(self,_query):
		gStart = time.time()
		_query['query'] = _query['query'].replace(' ','_')
		cont = subprocess.run(['aLine','-q',_query['query'],'-d','/dados/'+_query['database']],stdout=subprocess.PIPE).stdout.decode('utf-8')
		cont = cont.split('\n')
		_query['dateto'] = 'NULL'
		_query['datefrom'] = 'NULL'
		report = {}
		documents = []
		if _query['dateto'] != 'NULL' or _query['datefrom'] != 'NULL':

			if 	_query['dateto'] != 'NULL':
				size = _query['dateto'].split('/')
				size = len(size)
			elif _query['datefrom'] != 'NULL':
				size = _query['datefrom'].split('/')
				size = len(size)


			for i in content:
				i = i.replace('  ',' ')
				i = i.split(' ')

				if len(i) > 2:
					aux = i[0].split('/')
					t = ''
					for j in reversed(range(0,size)):
						t += aux[j]+'/'
					data = t[:-1]
					t = t.replace('/','')
					flag = False


					if _query['datefrom'] != 'NULL' and _query['dateto'] != 'NULL':

						#DATA DE INICIO
						a  = _query['datefrom'].replace('/','')
						#DATA DE FIM
						b = _query['dateto'].replace('/','')
						if  a >= t and b <= t:
							flag = True
						print(t,a,b)
					elif _query['datefrom'] != 'NULL':
						if not t >= (_query['datefrom'].replace('/','')):
							continue


					elif  _query['dateto'] != 'NULL':
						if not t <= (_query['dateto'].replace('/','')):
							continue
					flag = True


					if flag:
						doc = {}
						doc['document'] = i[0]
						doc['rank'] = float(i[2])
						doc['data'] = ' '
						documents.append(doc)
						if data in report:
							report[data] += 1
						else:
							report[data] = 1


		else:
			size = 2
			for i in cont:
				i = i.replace('  ',' ')
				i = i.split(' ')

				if len(i) > 2:
					aux = i[0].split('/')
					t = ''
					for j in reversed(range(0,size)):
						t += aux[j]+'/'
					data = t[:-1]
					t = t.replace('/','')
					doc = {}
					doc['document'] = i[0]
					doc['rank'] = float(i[2])
					doc['data'] = ' '
					documents.append(doc)
					if data in report:
						report[data] += 1
					else:
						report[data] = 1
					#t = int(t)
		# for i in cont:
		# 	i = i.replace('  ',' ')
		# 	i = i.split(' ')
		# 	if len(i) > 2:
		# 		#print(i)
		# 		doc = {}
		# 		doc['document'] = i[0]
		# 		doc['rank'] = float(i[2])
		# 		doc['data'] = ' '
		# 		report[i[1]] = doc

		# 	#ADICIONAR MAIS DADOS NESSE JSON
		gEnd = time.time()
		elapsed = gEnd - gStart
		
		return JsonResponse({'report':report,'documents':documents,'time':elapsed})

	def info(self):
		name  = subprocess.run(['hostname'],stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
		ip  = subprocess.run(['hostname','-I'],stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')[0].split(' ')
		data = {
		'name':name[0],
		'ip':ip[0]
		}


		return JsonResponse(data)

def slave(request):
	if request.method == 'GET':
		action = request.GET.get('action')
		if action == 'search':	#slave/?action=search&query=conciliar&database=atribuna
			database = request.GET.get('database')
			_query = {}
			_query['query'] = request.GET.get('query')
			_query['database'] = request.GET.get('database')
			slave = Slave("name")
			return HttpResponse(slave.search(_query), content_type="application/json")
		elif action == 'report': #slave/?action=report&query=conciliar&database=atribuna&datefrom=01/2017&dateto=03/2017

			database = request.GET.get('database')
			_query = {}
			_query['query'] = request.GET.get('query')
			_query['database'] = request.GET.get('database')
			_query['datefrom'] = request.GET.get('datefrom')
			_query['dateto'] = request.GET.get('dateto')

			slave = Slave("name")
			return HttpResponse(slave.report(_query), content_type="application/json")
		elif action == 'info': #slave/?action=info
			slave = Slave('name')
			return(slave.info())
