from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter
import subprocess
import threading
import time
import requests
import operator


from datetime import datetime


def log(_obj):	
	import json
	import uuid		
	obj = json.dumps(_obj)		
	
	u = uuid.uuid1()
	id = u.urn.split(':')[-1]
	
	with open('/dados/logs/'+id,'w') as f:
		f.write(obj)

def extract_rank(json):
	try:
		return int(json['rank'])
	except KeyError:
		return 0

def readIps(_file):
	ips = []
	with open(_file) as f:
		content  = f.read().split('\n')
		for ip in content:
			if ip:
				ips.append(ip)
	return ips

def jobs(_ips,_query):
	threads = []

	for i,ip in enumerate(_ips):
		search = Node("Nodo-"+str(i),ip,_query)
		search.start()
		threads.append(search)

	result = {}
	times = {}
	

	#JUNTADOS OS RESULTADOS DAS THREADS
	for t in threads:
		t.join()
		result[t.name] = t.resp

	aux = {}
	aux['report'] = {}
	aux['documents'] = []
	for node in result:

		#print(result[node])
		for doc in result[node]['documents']:
			
			aux['documents'].append(doc)
	
		for date in result[node]['report']:
			if date in aux['report']:
				aux['report'][date] += result[node]['report'][date]
			else:
				aux['report'][date] = result[node]['report'][date]
		temp = []
	for date in aux['report']:
		x = [ date ,aux['report'][date]]
		# x = [ datetime.strptime(date, '%m/%Y').strftime("%m-%Y") ,result[node]['report'][date]]
		# x['date'] = date
		# x['total'] = result[node]['report'][date]
		temp.append(x)
	temp.sort(key=itemgetter(1), reverse=True)
	temp.sort(key=lambda L: datetime.strptime(L[0], '%m/%Y'))
	date = []
	cont = []
	for i,j in temp:
		date.append(i)
		cont.append(j)
		
	aux['report'] = {
		'date' : date,
		'cont' : cont,
	}
		
			

	
	aux['documents'].sort(key = extract_rank)
	
	return aux


class Node (threading.Thread):
	def __init__(self, name, _ip,_query):
		threading.Thread.__init__(self)
		self.name = name
		self.query = _query
		self.result = {}
		self.ip = _ip
		self.resp = {}

	def run(self):
		threadLock = threading.Lock()
	 	# Get lock to synchronize threads
		threadLock.acquire()

		if self.query['action'] == 'search':
			# f = requests.get('http://'+self.ip+'/slave/?action=search&query='+self.query['query']+'&database='+self.query['database'])
			# f = requests.get('http://'+self.ip+'/?query='+self.query['query']+'&database='+self.query['database']+'&dateto='+self.query['dateto']+'&datefrom='+self.query['datefrom'])
			
			url = 'http://{}/?{}'.format(self.ip,self.query['urlencode'])

			#url = 'http://{}/?action=search&query={}&database={}'.format(self.ip,self.query['query'],self.query['database'])
			# f = requests.get('http://'+self.ip+'/?query='+self.query['query'])
			print(url)
			f = requests.get(url)

			if(f.json()['status']):
				self.resp  = f.json()
		elif self.query['action'] == 'report':
			f = requests.get('http://'+self.ip+'/slave/?action=report&query='+self.query['query']+'&database='+self.query['database']+'&dateto='+self.query['dateto']+'&datefrom='+self.query['datefrom'])
			self.resp = f.json()

		elif self.query['action'] == 'info':
			f = requests.get('http://'+self.ip+'/slave/?action=info')
			self.resp = f.json()



	  # Free lock to release next thread
		threadLock.release()
class Broker(object): #broker/?action=report&query=conciliar&database=atribuna&dateto=01/2016
	"""docstring for Broker"""
	def __init__(self, arg):
		super(Broker, self).__init__()
		self.arg = arg
		self.ips = ['localhost:9888/']
		#self.ips = ['10.9.8.251:9000']
		# self.ips = readIps("/home/master/ips.txt")
		

	def search(self,_query):
		threads = []

		for i,ip in enumerate(self.ips):
			search = Node("Nodo-"+str(i),ip,_query)			
			search.start()
			threads.append(search)

		result = {}
		times = {}
		gStart = time.time()

		#JUNTADOS OS RESULTADOS DAS THREADS
		for t in threads:
			t.join()
			result[t.name] = t.resp

		aux = {}
		aux['documents'] = []
		aux['total'] = 0
		
		for node in result:
			if len(result[node]) > 0:			
				aux['total'] += result[node]['total']
				for doc in result[node]['result']:					
					aux['documents'].append([doc['document'],doc['link'],doc['rank']])
				
		
		aux['documents'] = sorted(aux['documents'], key=lambda d: d[2],reverse=True)
		aux['documents'] = [[x[0],x[1]] for x in aux['documents']]
		gEnd = time.time()
		elapsed = gEnd - gStart
		aux['time'] = elapsed
		
		# print('Broker - Time elapsed:',aux['time'])

		return(JsonResponse(aux))
	def report(self,_query):
		# threads = []
		# for i,ip in enumerate(self.ips):
		# 	search = Node("Nodo-"+str(i),ip,_query)
		# 	search.start()
		# 	threads.append(search)

		# result = {}
		# times = {}
		# gStart = time.time()

		# #JUNTADOS OS RESULTADOS DAS THREADS
		# for t in threads:
		# 	t.join()
		# 	result[t.name] = t.resp
		gStart = time.time()
		querys = _query['query'].split('_')
		temp = {}
		result = {}
		result['report'] = {}
		# aux['report'] = {}
		result['documents'] = []
		for i in querys:
			_query['query'] = i
			
			q = jobs(self.ips,_query)	
			
			result['report'][i] = q['report']
			
			for doc in q['documents']:
				if not doc in result['documents']:					
					result['documents'].append(doc)
			
			# for node in q:
			# #print(result[node])
			# 	for doc in q[node]['documents']:
			# 		aux['documents'].append(doc)
			# 	for date in result[node]['report']:
			# 		if date in aux['report']:
			# 			aux['report'][date] += q[node]['report'][date]
			# 		else:
			# 			aux['report'][date] = q[node]['report'][date]

			# aux['documents'].sort(key = extract_rank)
			# result['repot'] = aux
	

		# aux = {}
		# aux['report'] = {}
		# aux['documents'] = []
		# for node in result:
		# 	#print(result[node])
		# 	for doc in result[node]['documents']:
		# 		aux['documents'].append(doc)
		# 	for date in result[node]['report']:
		# 		if date in aux['report']:
		# 			aux['report'][date] += result[node]['report'][date]
		# 		else:
		# 			aux['report'][date] = result[node]['report'][date]

		# aux['documents'].sort(key = extract_rank)

		gEnd = time.time()
		elapsed = gEnd - gStart
		result['time'] = elapsed

		return(JsonResponse(result))
	def info(self,_query):
		threads = []
		result = {}
		times = {}
		for i,ip in enumerate(self.ips):
			search = Node("Nodo-"+str(i),ip,_query)
			search.start()
			threads.append(search)
		for t in threads:
			t.join()
			result[t.name] = t.resp


		return(JsonResponse(result))



@csrf_exempt
def broker(request):
	if request.method == 'GET':
		action = request.GET.get('action')
		# print(request.build_absolute_uri())
		
		if action == 'search':
			_query = {}
			_query['action'] = request.GET.get('action')
			_query['urlencode'] = request.GET.urlencode()
			broker = Broker('name')
			return(broker.search(_query))
		elif action == 'report':
			_query = {}
			_query['action'] = request.GET.get('action')
			_query['query'] = request.GET.get('query')
			_query['database'] = request.GET.get('database')
			if request.GET.get('datefrom'):
				_query['datefrom'] = request.GET.get('datefrom')
			else:
				_query['datefrom'] = 'NULL'
			if request.GET.get('dateto'):
				_query['dateto'] = request.GET.get('dateto')
			else:
				_query['dateto'] = 'NULL'
			broker = Broker('name')
			return(broker.report(_query))
		elif action == 'info':
			_query = {}
			_query['action'] = request.GET.get('action')
			broker = Broker('name')

			return(broker.info(_query))
