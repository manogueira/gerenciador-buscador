#!/bin/bash

#INSTALAR O PACOTES DO PYTHON PARA 
#DJANGO

pwd
path=`pwd`
_PATH_WSGI_="$path/gerenciador"
_PROJETO_="gerenciador"
_PATH_VIRTUAL_ENV_="$path/aLine"
_PATH_DJANGO_MANEGER_="$path/gerenciador"


# vHost="<VirtualHost *:80>
	
# 	ServerAdmin webmaster@localhost
	
# 	ErrorLog ${APACHE_LOG_DIR}/error.log
# 	CustomLog ${APACHE_LOG_DIR}/access.log combined
	
# 	<Directory $_PATH_WSGI_>
#         <Files wsgi.py>
#             Require all granted
#         </Files>
#     </Directory>

#     WSGIDaemonProcess $_PROJETO_ python-home=$_PATH_VIRTUAL_ENV_ python-path=$_PATH_DJANGO_MANEGER_
#     WSGIProcessGroup $_PROJETO_
#     WSGIScriptAlias / $_PATH_WSGI_/wsgi.py
	

# </VirtualHost>
# "
vHost="<VirtualHost *:80>
	
	ServerAdmin webmaster@localhost
	
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	
	<Directory $_PATH_WSGI_>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess $_PROJETO_  python-path=$_PATH_DJANGO_MANEGER_
    WSGIProcessGroup $_PROJETO_
    WSGIScriptAlias / $_PATH_WSGI_/wsgi.py
	

</VirtualHost>
"



echo "$vHost" > /etc/apache2/sites-available/teste.conf